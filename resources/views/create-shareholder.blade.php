@extends('layouts.main')

@section('title')
    Add Shareholder
@endsection

@section('content')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

    <form action="/store" method="post" class="mt-4 p-4">
        <h3 align="center">Add Shareholder</h3>
        @csrf
        <div class="form-group m-3">
            <label for="name">Name</label>
            <input type="text" class="form-control" name="name">
        </div>
        <div class="form-group m-3">
            <label for="mobile-number">Mobile Number</label>
            <input type="text" class="form-control" name="mobile">
        </div>
        <div class="form-group m-3">
            <label for="email">Email</label>
            <input type="text" class="form-control" name="email">
        </div>
       
        <div class="form-group m-3">
            <label for="country">Country</label>
            <input type="text" class="form-control" name="country">
        </div>
        <div class="form-group m-3">
            <input type="submit" class="btn btn-primary align-center" value="Create">
            <input type="submit" class="btn btn-primary align-center" value="Reset">
        </div>
    </form>

@endsection