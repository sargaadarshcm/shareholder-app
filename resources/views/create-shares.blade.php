@extends('layouts.main')

@section('title')
    Add Shares
@endsection

@section('content')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

    <form action="/storeshare/{{ $id }}" method="post" id="#form" class="mt-4 p-4">
        <h3 align="center">Share Amount  Details</h3>
        @csrf
        <div class="conatainer">
        <div class="form-group m-3">
            <label for="duration">Duration-Year</label>
            <select name="duration" class="form-control" id="duration" onchange="calculation()">
                <option value="">Select</option>
                @for($i=1;$i<=5;$i++)
                <option value={{ $i }}>{{ $i }} Year</option>
                @endfor
               </select>
        </div>
        <div class="form-group m-3">
            <label for="total_amount">Total Amount-Year</label>
            <input type="text" class="form-control" name="total_amount" id="total-amount" onkeyup="calculation()">
        </div>
        <div class="form-group m-3">
            <label for="installment_type">Installment Type</label>
           <select name="installment_type" class="form-control" id="installment-type" onchange="calculation()">
            <option value="">Select</option>
            <option value="12">Monthly</option>
            <option value="4">Quarterly</option>
            <option value="2">Half-yearly</option>
            <option value="1">Custom</option>
           </select>
        </div>
       
        <div class="form-group m-3">
            <label for="first_installment">First Installment Start On</label>
            <input type="date" class="form-control" name="first_installment" id="first-installment" onchange="calculation()">
        </div>
        </div>
        <div class="container">
            <table border="0" cellpadding="2" width="100%" id='installmentDetails'> </table>
        </body>
        </div>
        <div class="form-group m-3">
            <input type="submit" class="btn btn-primary align-center" value="Create">
            <input type="submit" class="btn btn-primary align-center" value="Reset">
        </div>
    </form>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>  
    <script>
        $(document).ready(function () {

            $.myFunction = function(){ 
           
               
                var duration=$('#duration').val();
                var type=$('#installment-type').val();
                var start_date=$('#first-installment').val();
                var format_date=new Date($('#first-installment').val());
               
                var total_amount=$('#total-amount').val();
                if(duration && type && start_date && total_amount)
                {
                var nodays=type*duration;
                var amount=total_amount/nodays
                var content='<tr><th>Due Date</th><th>Amount</th></tr>';
                var first_date=moment(start_date).format('DD-MM-YYYY');
                for (var i=0;i<nodays;i++) {
                    if(type=="4")//Quartely
                {   
                    
                    content+="<tr><td><input type='text' id='date['"+i+"']' name='date['"+i+"']' value='"+first_date+"'></td><td><input type='text' id='amount' name='amount'  value='"+amount+"'></td></tr>"
                    format_date.setMonth(format_date.getMonth() + 3);
                    first_date = moment(format_date).format('DD-MM-YYYY');  
                }
                else if(type=="12")
                {
                content+="<tr><td><input type='text' id='date['"+i+"']' name='date['"+i+"']' value='"+first_date+"'></td><td><input type='text' id='amount' name='amount' value='"+amount+"'></td></tr>"
                format_date.setMonth(format_date.getMonth() + 1);
                first_date = moment(format_date).format('DD-MM-YYYY');  
                }
                  
                }
                $("#installmentDetails").html(content);
                }
            };
        });
        function calculation() {
         
         // Calling the jquery function with Javscript function
            $.myFunction();
         };
    </script>
@endsection