<?php

namespace App\Http\Controllers;

use App\Models\ShareDetail;
use App\Models\ShareHolder;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class ShareHolderController extends Controller
{
    //


    public function index(Request $request)
    {
        if ($request->ajax()) {
            $todos = ShareHolder::select('name', 'mobile', 'email', 'country', 'id')->get();
            return Datatables::of($todos)
                ->addColumn('action', function ($row) {
                    
                        $btn = "<a href='/getshare/$row->id' data-mdb-toggle='tooltip' title='Edit'>Share Details</a>";
                   
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('index');
    }


    public function store(Request $request)
    {



        $request->validate([
            'name' => 'required',
            'mobile' => 'required|numeric|digits:10',
            'email' => 'required|email',
            'country' => 'required'
        ]);


        $shareholder = new ShareHolder();
        $shareholder->name = $request->name;
        $shareholder->mobile = $request->mobile;
        $shareholder->email = $request->email;
        $shareholder->country = $request->country;
        $shareholder->save();
        session()->flash('success', 'Shareholder created succesfully');
        return redirect('/create');
    }

    public function getShares($id)
    {
        return view('create-shares',compact('id'));
    }

    public function storeShares(Request $request,$id)
    {
        $request->validate([
            'duration' => 'required',
            'total_amount' => 'required|numeric',
            'installment_type' => 'required',
            'first_installment' => 'required'
        ]);

        
        $share = new ShareDetail();
        $share->user_id = $id;
        $share->duration = $request->duration;
        $share->installment_type = $request->installment_type;
        $share->total_amount = $request->total_amount;
        $share->start_date = $request->first_installment;
        $share->installment_details = $request->amount;
        $share->save();
        session()->flash('success', 'Share created succesfully');
        return redirect('/index');
      
    }
}
