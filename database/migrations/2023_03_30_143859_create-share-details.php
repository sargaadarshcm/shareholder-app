<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('share_details', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->integer('duration');
            $table->string('installment_type');
            $table->integer('total_amount');
            $table->string('start_date');
            $table->text('installment_details');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('share_details');
    }
};
