@extends('layouts.main')
@section('title')
    Shareholder App
@endsection
@section('content')

  
            <table class="table yajra-datatable">
                <thead>
                  <tr>
                    <th scope="col">Name</th>
                    <th scope="col">Mobile Number</th>
                    <th scope="col">Email</th>
                    <th scope="col">Country</th>
                    <th scope="col">Actions</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
        
              <div class="container">
       
                <a href="/create"><span class="btn btn-primary">Add Shareholder</span></a>
            </div>
    
    <script type="text/javascript">
     
        $(document).ready(function () {
           
            $(function () {
                var table = $('.yajra-datatable').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: "/index",
                    paging:false,
                    columns: [
                        {data: 'name'},
                        {data: 'mobile'},
                        {data: 'email'},
                        {data: 'country'},
                        {data: 'action', name: 'action', orderable: false, searchable: false},
                    ]
                });
            });

        });
      
    </script>
@endsection
